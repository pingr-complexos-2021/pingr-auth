// External libraries
const request = require("supertest");
const bcrypt = require("bcrypt");

const jwt = require("jsonwebtoken");
const cookie = require('cookie');

// API module
const API = require("../src/api");

describe("The project", () => {
  let api, mockColl, mockDb, mongoClient, stanConn;
  const corsOptions = { origin: "*" };

  const secret = "712OculosEscuros!#";

  beforeEach(() => {
    mockColl = {
      insertOne: jest.fn(),
      findOne: jest.fn(),
      deleteOne: jest.fn(),
    };

    mockDb = { collection: () => mockColl };

    mongoClient = { db: () => mockDb };

    stanConn = { publish: jest.fn() };

    api = API(corsOptions, { mongoClient, stanConn, secret });
  });

  it("can use Jest", () => {
    expect(true).toBe(true);
  });

  it("can use CORS", async () => {
    const response = await request(api).get("/");
    const cors_header = response.header["access-control-allow-origin"];
    expect(cors_header).toBe("*");
  });

  //testes de Login
  it("valida dados corretos", async () => {
    let pwPlainText = '12356abcde';
    let pwHash = await bcrypt.hash(pwPlainText, 10);
    const newUser = {
      username_email: 'foo',
      //username_email: 'foo@example.com',
      password: pwPlainText,
      id: 42
    }
    mockColl.findOne.mockResolvedValueOnce({
      username: 'foo',
      email: 'foo@example.com',
      password: pwHash,
      id: 42
    })
    const response = await request(api).post("/login").send(newUser);
    
    var cookies = cookie.parse(response.get('Cookie-Set'));
    console.log(jwt.verify(cookies.sessionId, secret).id);
    
    expect(mockColl.findOne).toHaveBeenCalled();
    expect(mockColl.findOne).toReturn();
    expect(response.status).toBe(200);
  });
  
  /* precisamo esperar o pair que vai deixar essa funcao no common
  it("teste campos vazios", async () => {
    mockColl.findOne.mockResolvedValueOnce({
      username: '',
      email: '',
      password: '12356abcde',
      id: 42
    })
    const response = await request(api).post("/login");
    //analisa campos => {if achei um campo vazio -> espera status de res 400 | else espera status de res 400}
    expect(mockColl.findOne()).toHaveBeenCalled();
    expect(mockColl.findOne).toReturnTimes(2);
    expect(response.status).toBe(200);
  }); */


  it("valida dados incorretos", async () => {
    let pwPlainText = '12356abcde'
    let pwHash = await bcrypt.hash('123456abcdeErro', 10);
    const newUser = {
      username_email: 'foo',
      //username_email: 'foo@example.com',
      password: pwPlainText,
      id: 42
    }
    mockColl.findOne.mockResolvedValueOnce({
        username: 'foo',
        email: 'foo@example.com',
        password: pwHash,
        id: 42 
    })
    const response = await request(api).post("/login").send(newUser);
    expect(mockColl.findOne).toHaveBeenCalled();
    expect(mockColl.findOne).toReturn();
    expect(response.status).toBe(422);
  });
  //fim dos testes de Login

});
