const request = require("supertest");

/* IMPORT DA FUNÇÃO DE SIGNUP */
const API = require("../src/api");

describe("The user", () => {
    let api, mockColl, mockDb, mongoClient, stanConn;
    const corsOptions = { origin: "*" };
  
    const secret = "SUPERSECRET";
    mockColl = {
        insertOne: jest.fn(),
        findOne: jest.fn(),
        deleteOne: jest.fn(),
      };
  
    mockDb = { collection: () => mockColl };
  
    mongoClient = { db: () => mockDb };
  
    stanConn = { publish: jest.fn() };

    beforeEach(() => {
        api = API(corsOptions, {stanConn, mongoClient, secret});
    });


    it("should logout", async () => {

    const token =
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwOGVmNWNjMDY5MDIwYTFkNjFkNTM4MCJ9.DU55f1y8dGSJPWYXrHUUwU0zGc-N8FixQqontudI4RE";
  
      const response = await request(api)
        .post('/signout')
        .set('Cookie', [`token=${token}`]);

      expect(response.status).toBe(200);
      expect(response.headers['set-cookie'][0]).not.toContain(token);
  
    }); 
    
});