const request = require("supertest");

/* IMPORT DA FUNÇÃO DE SIGNUP */
const API = "dummy";

describe("The username", () => {
    it("should have more than 4 characters", () => {
        const pw = "Aaaa123456";
        const newUser = {
          username: "Foo",
          email: "foo@example.com",
          password: pw,
          passwordConfirmation: pw,
        };
    
        const response = await request(api).post("/signup").send(newUser);
    
        expect(response.status).toBe(400);
        expect(response.body.error).toBe(
          "Request body had malformed username"
        );
    }); 
    it("should have less than 16 characters", () => {
        const pw = "Aaaa123456";
        const newUser = {
          username: "Fooooooooooooooooooo",
          email: "foo@example.com",
          password: pw,
          passwordConfirmation: pw,
        };
    
        const response = await request(api).post("/signup").send(newUser);
    
        expect(response.status).toBe(400);
        expect(response.body.error).toBe(
          "Request body had malformed username"
        );
    }); 
    it("should have only letters, numbers and _", () => {
        const pw = "Aaaa123456";
        const newUser = {
          username: "Foooo-",
          email: "foo@example.com",
          password: pw,
          passwordConfirmation: pw,
        };
    
        const response = await request(api).post("/signup").send(newUser);
    
        expect(response.status).toBe(400);
        expect(response.body.error).toBe(
          "Request body had malformed username"
        );
    }); 
    it("should be required", () => {
        const pw = "Aa123456789";
        const newUser = {
          username: "",
          email: "foo@example.com",
          password: pw,
          passwordConfirmation: pw,
        };
    
        const response = await request(api).post("/signup").send(newUser);
    
        expect(response.status).toBe(400);
        expect(response.body.error).toBe(
          "Request body had malformed username"
          );
        });
        it("should be unique", () => {}); 
});

describe("The e-mail", ()=>{
    it("should be valid", ()=>{
        const pw = "Aaaa123456";
        const newUser = {
          username: "Fooooo",
          email: "fooexamplecom",
          password: pw,
          passwordConfirmation: pw,
        };
    
        const response = await request(api).post("/signup").send(newUser);
    
        expect(response.status).toBe(400);
        expect(response.body.error).toBe(
          "Request body had malformed e-mail"
        );
    });
    it("should be required", ()=>{
        const pw = "Aa123456789";
        const newUser = {
          username: "Foooo",
          email: "",
          password: pw,
          passwordConfirmation: pw,
        };
    
        const response = await request(api).post("/signup").send(newUser);
    
        expect(response.status).toBe(400);
        expect(response.body.error).toBe(
          "Request body had malformed e-mail"
        );
      });
    it("should be unique", ()=>{});
});

describe("The password", ()=>{
    it("should have more than 7 characters", ()=>{
        const pw = "Aa3456";
        const newUser = {
          username: "Fooooo",
          email: "foo@example.com",
          password: pw,
          passwordConfirmation: pw,
        };
    
        const response = await request(api).post("/signup").send(newUser);
    
        expect(response.status).toBe(400);
        expect(response.body.error).toBe(
          "Request body had malformed password"
        );
    });
    it("should have less than 65 characters", ()=>{
        const pw = "Aaaaaaaaaaaaaaa2aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
        const newUser = {
          username: "Foooo",
          email: "foo@example.com",
          password: pw,
          passwordConfirmation: pw,
        };
    
        const response = await request(api).post("/signup").send(newUser);
    
        expect(response.status).toBe(400);
        expect(response.body.error).toBe(
          "Request body had malformed password"
        );
    });
    it("should have at least one letter", ()=>{
        const pw = "123456789";
        const newUser = {
          username: "Foooo",
          email: "foo@example.com",
          password: pw,
          passwordConfirmation: pw,
        };
    
        const response = await request(api).post("/signup").send(newUser);
    
        expect(response.status).toBe(400);
        expect(response.body.error).toBe(
          "Request body had malformed password"
        );
    });
    it("should have at least one capital letter", ()=>{
        const pw = "aa123456789";
        const newUser = {
          username: "Foooo",
          email: "foo@example.com",
          password: pw,
          passwordConfirmation: pw,
        };
    
        const response = await request(api).post("/signup").send(newUser);
    
        expect(response.status).toBe(400);
        expect(response.body.error).toBe(
          "Request body had malformed password"
        );
    });
    it("should have one number", ()=>{
        const pw = "Aaaaaaaaaaaa";
        const newUser = {
          username: "Foooo",
          email: "foo@example.com",
          password: pw,
          passwordConfirmation: pw,
        };
    
        const response = await request(api).post("/signup").send(newUser);
    
        expect(response.status).toBe(400);
        expect(response.body.error).toBe(
          "Request body had malformed password"
        );
    });
    it("should have be required", ()=>{
        const pw = "";
        const newUser = {
          username: "Foooo",
          email: "foo@example.com",
          password: pw,
          passwordConfirmation: pw,
        };
    
        const response = await request(api).post("/signup").send(newUser);
    
        expect(response.status).toBe(400);
        expect(response.body.error).toBe(
          "Request body had malformed password"
        );
    });
});

describe("The password confirmation", ()=>{
    it("should be equal to password", ()=>{
        const pw = "Aa123456789";
        const newUser = {
          username: "Foooo",
          email: "foo@example.com",
          password: pw,
          passwordConfirmation: "Aaa123456789",
        };
    
        const response = await request(api).post("/signup").send(newUser);
    
        expect(response.status).toBe(400);
        expect(response.body.error).toBe(
          "Request body had malformed password confirmation"
        );
    });
});

describe("The id", ()=>{
    it("should be generated", ()=>{});
    it("should be unique", ()=>{});
});

describe("The signup function", ()=>{
    it("should save user on database", ()=>{});
    it("should post a user.registered event with id", ()=>{});
    it("should post a user.{ID} event with e-mail", ()=>{});
    it("should post a user.{ID} event with username", ()=>{});
    it("should redict the user to login endpoint", ()=>{});
})