//const jwt = require("jsonwebtoken");
//const { v4: uuid } = require("uuid");

const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid");
var cookieParser = require('cookie-parser');

const signoutHandler = require("./endpoints/signout/signout.js"); 
const signupHandler = require('./endpoints/signup/signup.js')
const LOGIN = require('./endpoints/login/login')

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));
  api.use(cookieParser())

  const db = mongoClient.db('mockDb'); //pegar um database do cliente mongo
  const coll = db.collection('mockColl'); //pegar uma colecao em especifico do database

  const {loginHandler} = LOGIN(stanConn, coll, secret);

  api.get("/", (req, res) => res.json("Hello, World!"));
  api.post("/signup", signupHandler(stanConn, coll)) //nats e mongo
  api.post("/login", loginHandler); //mongo
  api.post("/signout", signoutHandler); 
  api.post("/update", (req, res) => res.json("Hello, World!")); //nats e mongo
  api.delete("/delete", (req, res) => res.json("Hello, World!")); //nats e mongo

  return api;
};

