const bcrypt = require("bcrypt");

const SALT_ROUNDS = 10

module.exports = encryptPassword = password => {
    return bcrypt.hashSync(password, SALT_ROUNDS)
}
