const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt"); //npm install bcrypt
const cookie = require('cookie');

module.exports = function (stanConn, coll, secret){

const loginHandler = async function (req, res, next){
    const username_emailInserido = req.body.username_email //depende de como vai ser recebido pelo front end
    const passwordInserido = req.body.password
    
    let foundUser;

	//Inicio Login
    if(username_emailInserido.includes('@')){ //distingue campo1 entre email e username
      foundUser = await coll.findOne({email: username_emailInserido});
    }else{
      foundUser = await coll.findOne({username: username_emailInserido});
    }
   
    if(foundUser){
      if(await bcrypt.compare(passwordInserido, foundUser.password)){
        //caso de td certo :)
		
        const token = jwt.sign({id: foundUser.id}, secret);
        let date = new Date();
        let expiration = date.getTime() + (90*24*60*60*1000)

        res.setHeader('Cookie-Set', cookie.serialize('sessionId', token, {httpOnly: true, maxAge: expiration})) ;
        res.setHeader('Location', req.headers.referer || '/login');

        res.status(200).json(foundUser); //res.redirect("/home"); 
      }else{
        res.status(422).json({"error": "Password confirmation did not match."})
      }
    }else{
      res.status(404).json({"error": "User not found"})
    } 
  //Fim de Login
  next()
  }

  return {loginHandler}
};
