const encryptPassword = require('../../common/encryptPassword.js')
const { v4: uuid } = require("uuid")

module.exports = function (stanConn, coll){ 
    const signupHandler = async (req, res) => {
        const { username, email, password, passwordConfirmation } = req.body
        let error = []
        error.concat(validateUsername(username))
        error.concat(validateEmail(email))
        error.concat(validatePassword(password))
        error.concat(validatePasswordConfirmation({ password, passwordConfirmation }))

        if(error.length > 0){
            res.status(400)
            return false
        }

        const passwordEncrypted = encryptPassword(password)
        
        let id = uuid()
        while(foundUser = await coll.findOne({ id })) {
            id = uuid()
        }

        const userData = {
            id,
            username,
            email,
            password: passwordEncrypted
        }

        coll.insertOne(userData, function(error, response){
            if(error){
                res.status(400)
                return {error: "definir error"}
            }
            stanConn.publish(
                "users.registered",
                JSON.stringify({
                  id
                })
            )
            stanConn.publish(
                `users.${id}`,
                JSON.stringify({
                    key: "email",
                    value: email
                })
            )
            stanConn.publish(
                `users.${id}`,
                JSON.stringify({
                    key: "username",
                    value: username
                })
            )
            res.redirect(201, '/login')
        })
    }
    return signupHandler
}

const validateUsername = username => {
    let error = []
    const usernameFormat = /^[a-zA-Z0-9_]+$/

    if ( !usernameFormat.test(username) )
        error.append('formatError')

    if( username.length < 5 )
        error.append('minLengthError')

    if( username.length > 16 )
        error.append('maxLengthError')

    return error
}

const validateEmail = email => {
    let error = []
    const emailFormat = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    if ( !emailFormat.test(email) )
        error.append('formatError')

    return error
}

const validatePassword = password => {
    let error = []
    const passwordFormat = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{3,}$/

    if ( !passwordFormat.test(password) )
        error.append('formatError')

    if( password.length < 8 )
        error.append('minLengthError')

    if( password.length > 65 )
        error.append('maxLengthError')

    return error
}

const validatePasswordConfirmation = ({ password, passwordConfirmation }) => {  
    let error = []

    if (password != passwordConfirmation)
        error.append('notEqual')

    return error
}