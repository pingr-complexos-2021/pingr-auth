const signoutHandler = (req, res) => {
    res.status(200).clearCookie("token").send();
};

module.exports = signoutHandler;